'use strict';
const moduleUtils = require('../utils/modules');
const answersUtils = require('../utils/answers');

module.exports = {

    /**
     * Получить список модулей
     */
    getModulesPrompt() {
        return {
            type: 'checkbox',
            name: 'modules',
            message: 'Выбери используемые модули',
            choices: moduleUtils.getAvailableModules(),
            default: []
        };
    },

    /**
     * Получить название проекта
     */
    getSolutionName() {
        return {
            type: 'input',
            name: 'solutionName',
            message: 'Введите название проекта',
            default: 'MyBrand'
        };
    },

    /**
     * Получить название приложения
     */
    getApplicationName() {
        return {
            type: 'input',
            name: 'applicationName',
            message: 'Введите отображаемое название приложения',
            default: 'Лояльность'
        };
    },

    /**
     * Получить bundle (идентификатор) приложения
     */
    getApplicationId() {
        return {
            type: 'input',
            name: 'applicationId',
            message: 'Введите название идентификатор приложения',
            default: 'com.loymax.application'
        };
    },

    /**
     * Получить адрес сервера
     */
    getServerAddress() {
        return {
            type: 'input',
            name: 'server',
            message: 'Введите адрес сервера',
            default: 'http://public.rc.iis.local/api'
        };
    },

    /**
     * Получить состояние чата службы поддержки
     */
    getEnabledChat() {
        return {
            type: 'confirm',
            name: 'enableChat',
            message: 'Включить чат в службе поддержки?',
            default: true
        };
    },

    /**
     * Вывести выбранные ответы в консоль
     * @param {Array} answers ответы
     */
    logAllAnswers(answers) {
        answersUtils.logAnswers(answers);
        moduleUtils.logModulesAnswers(answers.modules);
    }
};