'use strict';
const Package = require('../package');
const ProjectStandart = require('../project-standart');
const fs = require('fs');
const chalk = require('chalk');
const spawnUtils = require('../utils/spawn');
const packageUtils = require('../utils/packages');
const projectStandartUtils = require('../utils/project');

module.exports = {

    /**
     * Установить пакеты в платформенные проекты
     * @param {Object} configuration конфигурация
     * @param {Array} modules список выбранных модулей
     */
    installPackage(configuration, modules) {
        console.log('Загрузка и установка пакетов в проект ');

        var configPackage = {
            nugether: configuration.nugether,
            solution: configuration.solutionFolder,
            id: 'Eto.Platfrom.iOS',
            version: '2.3.0',
            prerelease: true
        };

        spawnUtils.installPackage(configPackage);
    },

    /**
     * Подготовить проект примера с папками
     * @param {Object} configuration объект конфигурации
     */
    fetchAndPrepareSolution(configuration) {
        if (configuration == undefined || configuration == null) {
            console.log(chalk.red.bold('delegate.js:fetchAndPrepareSolution: configuration не задан'));
            return;
        }

        console.log('Копирование проекта из репозитория');
        spawnUtils.fetchRepository('tRZBp2pz6Aqcz5tmaPAR');
            
        console.log('Создание финальной папки ' + configuration.solution);
        fs.mkdirSync(configuration.solution);
      
        console.log('Создание временной папки ' + configuration.temp);
        fs.mkdirSync(configuration.temp);
    },

    /**
     * Удалить временные папки
     * @param {Object} configuration объект конфигурации
     */
    removeTemporaryFolders(configuration) {
        if (configuration == undefined || configuration == null) {
            console.log(chalk.red.bold('delegate.js:removeTemporaryFolders: configuration не задан'));
            return;
        }

        console.log('Удаление ' + configuration.tempFolder);
        spawnUtils.removeFolder(configuration.tempFolder);

        console.log('Удаление ' + configuration.reposFolder);
        spawnUtils.removeFolder(configuration.reposFolder);
    },

    /**
     * Запись ссылок модулей в packages.config
     * @param {Object} configuration объект конфигурации
     * @param {Array} modules список выбранных модулей
     */
    prepareReferenceForPackages(configuration, modules) {
        if (configuration == undefined || configuration == null) {
            console.log(chalk.red.bold('delegate.js:prepareReferenceForPackages: configuration не задан'));
            return;
        }

        // define packages.config file
        const iosPackagePath = configuration.solutionFolder + '/' + configuration.solution + '.iOS/packages.config';
        const droidPackagePath = configuration.solutionFolder + '/' + configuration.solution + '.Droid/packages.config';
        
        console.log(chalk.green.bold('Текущая версия пакетов в packages.config = ', packageUtils.getVersion(iosPackagePath)));
        console.log(chalk.green.bold('Добавляем модули в проекты'));
    
        var iosPackage = new Package(iosPackagePath, true);
        iosPackage.setModules(modules);
        iosPackage.writeReferencesToPackageConfig();
    
        var droidPackage = new Package(droidPackagePath, false);
        droidPackage.setModules(modules);
        droidPackage.writeReferencesToPackageConfig();
    },

    /**
     * Запись ссылок модулей в ядро
     * @param {Object} configuration объект конфигурации
     * @param {Array} modules список выбранных модулей
     */
    prepareReferenceForCoreProj(configuration, modules) {
        if (configuration == undefined || configuration == null) {
            console.log(chalk.red.bold('delegate.js:prepareReferenceForCoreProj: configuration не задан'));
            return;
        }

        // define packages.config file
        const coreProjectPath = configuration.solutionFolder + '/' + configuration.solution + '.Core/' + configuration.solution + '.Core.csproj';
        
        console.log(chalk.green.bold('Текущая версия пакетов в ядре = ', projectStandartUtils.getVersion(coreProjectPath)));
        console.log(chalk.green.bold('Добавляем модули в ядро'));

        var coreProject = new ProjectStandart(coreProjectPath);
        coreProject.setModules(modules);
        coreProject.writeReferenceToProject();
    },

    /**
     * Восстановить пакеты для всего решения
     * @param {Object} configuration объект конфигурации
     */
    restorePackages(configuration) {
        if (configuration == undefined || configuration == null) {
            console.log(chalk.red.bold('delegate.js:restorePackages: configuration не задан'));
            return;
        }

        console.log(chalk.green.bold('Восстанавливаем модули'));
        const sln = configuration.solutionFolder + '/' + configuration.solution + '.sln';
        spawnUtils.restorePackages(sln);
    },

    /**
     * Обновить пакеты в android и ios проектах
     * @param {Object} configuration объект конфигурации
     * @param {Array} modules список модулей
     */
    updatePackages(configuration, modules) {
        if (configuration == undefined || configuration == null) {
            console.log(chalk.red.bold('delegate.js:updatePackages: configuration не задан'));
            return;
        }
        
        const iosCsproj = configuration.solutionFolder + '/' + configuration.solution + '.iOS/' + configuration.solution + '.iOS.csproj';
        const droidCsProj = configuration.solutionFolder + '/' + configuration.solution + '.Droid/' + configuration.solution + '.Droid.csproj';

        console.log(chalk.green.bold('Обновляем модули для iOS'));
        // modules.forEach(module => {
        //     spawnUtils.updatePackages(iosCsproj, 'Loymax.Module.' + module);
        // });

        spawnUtils.updatePackages(iosCsproj, 'Loymax.Core')

        console.log(chalk.green.bold('Обновляем модули для Android'));
        // modules.forEach(module => {
        //     spawnUtils.updatePackages(droidCsProj, 'Loymax.Module.' + module);
        // });
    }
};