'use strict';
const Generator = require('yeoman-generator');

const chalk = require('chalk');
const yosay = require('yosay');
const rename = require('gulp-rename');
const _ = require('lodash');

const stringUtils = require('./utils/string');

const promptsHelper = require('./helpers/prompts');
const delegateHelper = require('./helpers/delegate');

module.exports = class extends Generator {

  constructor(args, opts) {
    super(args, opts);
    
    this.configOptions = {};

    // (Не используется) Добавляет поддержку аргумента `--skip-droid`
    this.option('skip-droid', {
      desc: 'Пропустить генерацию андроида',
      type: Boolean,
      defaults: false
    });

  }

  initializing() {

  }

  prompting() {
    
    this.log(yosay(
      'Welcome to the marvelous ' + chalk.blue('generator-mlc') + ' generator!'
    ));
    
    const prompts = [
      promptsHelper.getSolutionName(),
      promptsHelper.getApplicationName(),
      promptsHelper.getApplicationId(),
      promptsHelper.getServerAddress(),
      promptsHelper.getEnabledChat(),
      promptsHelper.getModulesPrompt()
    ];

    return this.prompt(prompts).then(answers => {
      // To access props later use this.props.someAnswer;
      this.props = answers;
      promptsHelper.logAllAnswers(this.props);      
    });
  }

  configuring() {
    this.configuration = {};
    this.configuration.solution = this.props.solutionName;
    this.configuration.solutionFolder = this.destinationPath(this.configuration.solution);

    this.configuration.temp = 'temp';
    this.configuration.tempFolder = this.destinationPath(this.configuration.temp);

    this.configuration.repos = 'mlc-template';
    this.configuration.reposFolder = this.destinationPath(this.configuration.repos);
  }

  writing() {

    delegateHelper.fetchAndPrepareSolution(this.configuration);

    const solution = this.props.solutionName;
    const tempPath = this.configuration.tempFolder;
    const solutionPath = this.configuration.solutionFolder; 
    const repoPath = this.configuration.reposFolder;
    
    this.log('Модификация проекта');  

    this.fs.copy(repoPath, tempPath, {
      process: function(content) {
          var namespaceRegex = new RegExp(/XLoyalty/, 'g');
          var applicationIdRegex = new RegExp(/com.application.identificator/, 'g');
          var applicationNameRegex = new RegExp(/ApplicationDisplayName/, 'g');
          var serverUrlRegex = new RegExp(/loymaxServerAppUrl/, 'g');
          var enableChatRegex = new RegExp(/supportServiceEnableChat/, 'g');

          return content.toString()
            .replace(namespaceRegex, '<%= namespaceContext %>')
            .replace(applicationIdRegex, '<%= applicationIdContext %>')
            .replace(applicationNameRegex, '<%= applicationNameContext %>')
            .replace(serverUrlRegex, '<%= serverUrlContext %>')
            .replace(enableChatRegex, '<%= enableChatContext %>');
      }
    });

    var stream = rename(function(path) {
      path.basename = path.basename.replace(/(XLoyalty)/g, solution);
      path.dirname = path.dirname.replace(/(XLoyalty)/g, solution);
    });

    stream.on('end', () => {
      this._completeProcess(this.configuration, this.props.modules);
    });

    this.registerTransformStream(stream);

    this.log('Проект копируется из ' + tempPath);
    this.log('Проект копируется в ' + solutionPath);
    this.fs.copyTpl(tempPath, solutionPath, {
      namespaceContext: this.props.solutionName,
      applicationIdContext: this.props.applicationId,
      applicationNameContext: this.props.applicationName,
      serverUrlContext: this.props.server,
      enableChatContext: stringUtils.capitalizeFirstLetter(this.props.enableChat),
    });
  }
  
  conflicts() {

  }

  install() {
    //this.installDependencies();
  }

  /**
   * Окончание потока
   * @param {Object} configuration объект конфигурации
   * @param {Array} modules список выбранных модулей
   */
  _completeProcess(configuration, modules) {   

    delegateHelper.removeTemporaryFolders(configuration);

    // добавляем модули в ядро
    delegateHelper.prepareReferenceForCoreProj(configuration, modules);

    // добавляем пакеты в платформенные проекты
    var nugether = this.sourceRoot() + '/../../nugether/nugether.exe';
    configuration.nugether = nugether
    delegateHelper.installPackage(configuration, modules);

    // delegateHelper.prepareReferenceForPackages(configuration, modules);

    // delegateHelper.restorePackages(configuration);

    // delegateHelper.updatePackages(configuration, modules);

  }

  end() {
    this.log(chalk.green.bold('Готовый проект в ', this.configuration.solutionFolder));
  }
};