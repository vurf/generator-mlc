'use strict';
const chalk = require('chalk');
const et = require('elementtree');
const fs = require('fs');
const formattor = require('formattor');
const projectUtils = require('./utils/project');

function ProjectStandart(projectPath) {
    this.projectPath = projectPath;
}

/**
 * Установить выбранные модули
 * @param {Array} modules список модулей
 */
ProjectStandart.prototype.setModules = function(modules) {
    const hasModule = mod => modules && modules.indexOf(mod) !== -1;

    this.hasSignUpModule = hasModule('SignUp');
    this.hasSignInModule = hasModule('SignIn');
    this.hasResetPasswordModule = hasModule('ResetPassword');
    this.hasOffers = hasModule('Offers');
    this.hasMerchants = hasModule('Merchants');
    this.hasShoppingList = hasModule('ShoppingList');
    this.hasNotifications = hasModule('Notifications');
    this.hasPurchaseHistory = hasModule('PurchaseHistory');
    this.hasProfile = hasModule('Profile');
    this.hasSupportService = hasModule('SupportService');   
}

/**
 * Добавить ссылки для модулей в Core.csproj
 */
ProjectStandart.prototype.writeReferenceToProject = function() {
    const project = this.projectPath;
    var data = fs.readFileSync(project).toString();
    var xml = et.parse(data);
    
    var parentPackageRef;
    var findedRefInclude;

    var itemGroups = xml.findall('./ItemGroup');
    for (var i = 0; i < itemGroups.length; i++) {        
        var neededItemGroup = itemGroups[i];
        var packageRefs = neededItemGroup.findall('./PackageReference')[0];
        if (packageRefs == undefined || packageRefs == null) {
            continue;
        }

        findedRefInclude = packageRefs.get('Include') == 'Loymax.Core';
        parentPackageRef = neededItemGroup;
    }

    if (findedRefInclude == false) {
        console.log(chalk.red.bold('Не найден пакет Loymax.Core'));
        return;
    }

    const version = projectUtils.getVersion(project);
    console.log(chalk.blue.bold('Используемая версия пакетов в Core.cspoj = ', version));

    var subElement = et.SubElement;

    if (this.hasSignUpModule) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.SignUp', version);
    }

    if (this.hasSignInModule) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.SignIn', version);
    }

    if (this.hasResetPasswordModule) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.ResetPassword', version);
    }

    if (this.hasOffers) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.Offers', version);
    }  
    
    if (this.hasMerchants) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.Merchants', version);
    }

    if (this.hasShoppingList) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.ShoppingList', version);
    }

    if (this.hasNotifications) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.Notifications', version);
    }

    if (this.hasPurchaseHistory) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.PurchaseHistory', version);
    }

    if (this.hasProfile) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.Profile', version);
    }

    if (this.hasSupportService) {
        projectUtils.includeReferenceFor(parentPackageRef, subElement, 'Loymax.Module.SupportService', version);
    }
       
    var formattedXml = formattor(xml.write(), {method: 'xml'});
    fs.writeFileSync(project, formattedXml, null); 
};

module.exports = ProjectStandart;