'use strict';
const et = require('elementtree');
const fs = require('fs');
const formattor = require('formattor');
const packageUtils = require('./utils/packages');

/**
 * Модель работы с packages.config
 * @param {String} packagePath путь к packages.config
 * @param {Boolean} ios используется ios 
 */
function Package(packagePath, ios) {
    this.packagePath = packagePath;
    this.isIos = ios; 
}

/**
 * Установить выбранные модули
 * @param {Array} modules список модулей
 */
Package.prototype.setModules = function(modules) {
    const hasModule = mod => modules && modules.indexOf(mod) !== -1;

    this.hasSignUpModule = hasModule('SignUp');
    this.hasSignInModule = hasModule('SignIn');
    this.hasResetPasswordModule = hasModule('ResetPassword');
    this.hasOffers = hasModule('Offers');
    this.hasMerchants = hasModule('Merchants');
    this.hasShoppingList = hasModule('ShoppingList');
    this.hasNotifications = hasModule('Notifications');
    this.hasPurchaseHistory = hasModule('PurchaseHistory');
    this.hasProfile = hasModule('Profile');
    this.hasSupportService = hasModule('SupportService');   
}

/**
 * Добавить ссылки для модулей в packages.config
 */
Package.prototype.writeReferencesToPackageConfig = function() {
    const project = this.packagePath;
    const data = fs.readFileSync(project).toString();
    var xml = et.parse(data);
    var subElement = et.SubElement;

    const version = packageUtils.getVersion(project);

    if (this.hasSignUpModule) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.SignUp', version, this.isIos);
    }

    if (this.hasSignInModule) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.SignIn', version, this.isIos);
    }

    if (this.hasResetPasswordModule) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.ResetPassword', version, this.isIos);
    }

    if (this.hasOffers) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.Offers', version, this.isIos);
    }  
    
    if (this.hasMerchants) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.Merchants', version, this.isIos);
    }

    if (this.hasShoppingList) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.ShoppingList', version, this.isIos);
    }

    if (this.hasNotifications) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.Notifications', version, this.isIos);
    }

    if (this.hasPurchaseHistory) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.PurchaseHistory', version, this.isIos);
    }

    if (this.hasProfile) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.Profile', version, this.isIos);
    }

    if (this.hasSupportService) {
        packageUtils.includePackageFor(xml, subElement, 'Loymax.Module.SupportService', version, this.isIos);
    }
    
    var formattedXml = formattor(xml.write(), {method: 'xml'});
    fs.writeFileSync(project, formattedXml, null); 
};

module.exports = Package;