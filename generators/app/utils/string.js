'use strict';

module.exports = {

    capitalizeFirstLetter(bool) {
        var string = bool.toString();
        if (typeof string !== 'undefined' && string !== null) {
          return string[0].toUpperCase() + string.slice(1);
        }
        
        return 'False';
    }
};