'use strict';
const chalk = require('chalk');

module.exports = {

    /**
     * Вывести список выбранных параметров проекта
     * @param {Array} answers ответы
     */
    logAnswers(answers) {
        console.log(chalk.green.bold('======================'));
        console.log(chalk.green.bold('Проверка выбранных параметров проекта'));
        console.log(chalk.green('Название проекта =  ', answers.solutionName));
        console.log(chalk.green('Отображаемое название приложения =  ', answers.applicationName));
        console.log(chalk.green('Идентификатор приложения =  ', answers.applicationId));
        console.log(chalk.green('Сервер =  ', answers.server));
        console.log(chalk.green('Выбранные модули =  ', answers.modules));
        console.log(chalk.green('Чат поддержки = ', answers.enableChat));
        console.log(chalk.green.bold('======================'));
    }
};