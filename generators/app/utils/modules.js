'use strict';
const chalk = require('chalk');

module.exports = {
    
    /**
     * Получить список всех доступных модулей
     */
    getAvailableModules() {
        return [
            { name: 'Авторизация', value: 'SignIn' },
            { name: 'Регистрация', value: 'SignUp' },
            { name: 'Восстановление пароля', value: 'ResetPassword' },
            { name: 'Акции', value: 'Offers' },
            { name: 'Магазины', value: 'Merchants' },
            { name: 'Список покупок', value: 'ShoppingList' },
            { name: 'Оповещения', value: 'Notifications' },
            { name: 'История покупок', value: 'PurchaseHistory' },
            { name: 'Личный кабинет', value: 'Profile' },
            { name: 'Служба поддержки', value: 'SupportService' },
        ];
    },

    /**
     * Вывести список выбранных модулей
     * @param {Array} modules список модулей
     */
    logModulesAnswers(modules) {
        const hasModule = mod => modules && modules.indexOf(mod) !== -1;

        console.log(chalk.green.bold('======================'));
        console.log(chalk.green.bold('Проверка выбранных параметров модулей'));
        console.log(chalk.green('Авторизация =  ', hasModule('SignIn')));
        console.log(chalk.green('Регистрация = ', hasModule('SignUp')));
        console.log(chalk.green('Восстановление пароля = ', hasModule('ResetPassword')));
        console.log(chalk.green('Акции = ', hasModule('Offers')));
        console.log(chalk.green('Магазины = ', hasModule('Merchants')));
        console.log(chalk.green('Список покупок = ', hasModule('ShoppingList')));
        console.log(chalk.green('Оповещения = ', hasModule('Notifications')));
        console.log(chalk.green('История покупок = ', hasModule('PurchaseHistory')));
        console.log(chalk.green('Личный кабинет = ', hasModule('Profile')));
        console.log(chalk.green('Служба поддержки = ', hasModule('SupportService')));
        console.log(chalk.green.bold('======================'));
    }
};