'use strict';
const et = require('elementtree');
const chalk = require('chalk');
const fs = require('fs');

module.exports = {

    /** Получить версию пакета в packages.config
    * @param {String} path - путь до packages.config
    */
    getVersion(path) {
        var data = fs.readFileSync(path).toString();
        var xml = et.parse(data);
    
        const nodePackageCore = xml.findall('*[@id="Loymax.Core"]')[0];
        if (nodePackageCore == undefined || nodePackageCore == null) {
            console.log(chalk.red.bold('Необходимо установить Loymax.Core, по нему определяется текущая версия пакетов'));
            return;
        }
    
        const version = nodePackageCore.get('version');
        if (version == undefined || version == null) {
            console.log(chalk.red.bold('У Loymax.Core отсутствует версия пакета.'));
            return;
        }
    
        return version;
    },
  
    /**
     * Добавить ссылку на пакет в packages.config с targetFramework платформы
     * @param {ElementTree} xml спарсенные данные xml
     * @param {SubElement} subElement подэлемент для поиска
     * @param {String} packageId идентификатор пакета
     * @param {String} version версия пакета
     * @param {Boolean} ios использовать targetFramework ios?
     */
    includePackageFor(xml, subElement, packageId, version, ios) {
        var item = subElement(xml.getroot(), 'package');
        item.set('id', packageId);
        item.set('targetFramework', ios ? 'xamarinios10' : 'monoandroid81');
        item.set('version', version);
    }
};