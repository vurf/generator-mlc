'use strict';
const spawn = require('cross-spawn');
const _ = require('lodash');

module.exports = {

    /**
     * Normalize a command across OS and spawn it (synchronously).
     *
     * @param {String} command program to execute
     * @param {Array} args list of arguments to pass to the program
     * @param {object} [opt] any cross-spawn options
     * @return {String} spawn.sync result
     */
    spawnCommandSync(command, args, opt) {
        opt = opt || {};
        return spawn.sync(command, args, _.defaults(opt, { stdio: 'inherit' }));
    },

    /**
     * Выполнить установку пакетов
     * @param {Object} config конфигурация для выполнения команды
     */
    installPackage(config) {

        this.spawnCommandSync('mono', [config.nugether, 
            '-p', config.solution, 
            '-i', config.id, 
            '-v', config.version,
            'prerelease']);
    },

    /**
     * Загрузить репозиторий
     * @param {String} token маркер доступа пользователя
     */
    fetchRepository(token) {
        var repo = 'https://username:' + token + '@gitlab.com/loymax-sample/mlc-template.git';
        this.spawnCommandSync('git', ['clone', repo]);
    },

    /**
     * Восстановить пакеты
     * @param {String} solution путь к решению
     */
    restorePackages(solution) {        
        // nuget restore XLoyalty.sln
        this.spawnCommandSync('nuget', ['restore', solution]);
    },

    /**
     * Обновить пакеты для проекта
     * @param {String} project путь к проекту
     * @param {String} packageId идентификатор пакета
     */
    updatePackages(project, packageId) {
        //nuget update XLoyalty.iOS/XLoyalty.iOS.csproj -Id Loymax.Core -PreRelease
        this.spawnCommandSync('nuget', ['update', project, '-Id', packageId, '-PreRelease']);
    },

    /**
     * Удалить папку и ее содержимое
     * @param {String} folder папка
     */
    removeFolder(folder) {
        this.spawnCommandSync('rm', ['-rf', folder]);
    }
}